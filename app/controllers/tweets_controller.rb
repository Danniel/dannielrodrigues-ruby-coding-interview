class TweetsController < ApplicationController
  def index
    tweets = Tweet.order(id: :desc).limit(5)
    tweets = tweets.where('id < ?', params[:last_item]) if params[:last_item]

    render json: tweets
  end
end
