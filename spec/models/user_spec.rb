require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:company) { create(:company) }
  let!(:user_1) { create(:user, company: company, username: 'user_1') }
  let!(:user_2) { create(:user, company: company, username: 'user_2') }
  let!(:user_3) { create(:user, company: company, username: 'person_3') }

  context 'when searching' do
    it 'returns the user when searching full username' do
      result = described_class.by_username('user_1')

      expect(result.size).to eq(1)
      expect(result.first.username).to eq('user_1')
    end

    it 'returns the users when searching partial username' do
      result = described_class.by_username('user_')

      expect(result.size).to eq(2)
    end

    it 'doesnt return when given unexisting names' do
      result = described_class.by_username('banana')

      expect(result.size).to eq(0)
    end
  end
end
